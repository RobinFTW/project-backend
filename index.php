<?php
//Epiphany
include_once 'lib/Epi.php';
Epi::setPath('base', 'lib');
Epi::setPath('config', dirname(__FILE__));
Epi::init('api');
Epi::init('config');

header('Access-Control-Allow-Origin: *');

// Load configuration
getConfig()->load('config');

// Load Database
Epi::init('database');
EpiDatabase::employ('mysql', getConfig()->get('dbname'), getConfig()->get('dbhost'), getConfig()->get('dbuser'), getConfig()->get('dbpass'));

// ROUTES
getApi()->get('/version.json', 'apiVersion', EpiApi::external);
getApi()->get('/tables.json', 'tables', EpiApi::external);

getApi()->get('/time.json', 'getTime', EpiApi::external);


// Get hour
getApi()->get('/getHour/(\d+)/json', 'getHour', EpiApi::external);
getApi()->get('/getImage/(\d+)/json', 'getImage', EpiApi::external);
getApi()->get('/getTwitter/(\d+)/json', 'getTwitterTag', EpiApi::external);
getApi()->get('/getNews/(\d+)/json', 'getNewsPost', EpiApi::external);
getApi()->get('/getWeather/(\d+)/json', 'getWeatherConditions', EpiApi::external);


// Post image
getApi()->post('/setImage', 'setImage', EpiApi::external);
// Get by filter

// Get tags


getRoute()->run();

// FUNCTIONS
function getTime(){
  return time();
}

function getHour($timestamp){
  //return unixToMYSQL($timestamp);
  return getDatabase()->one('SELECT * FROM nh_hour WHERE timestamp=:timestamp', array(':timestamp'=>unixToMYSQL($timestamp)));
}

function getImage($id){
  return getDatabase()->one('SELECT * FROM nh_image WHERE image_id=:image_id', array(':image_id'=> $id));
}

function getTwitterTag($id){
  return getDatabase()->one('SELECT * FROM nh_twitter WHERE twitter_id=:twitter_id', array(':twitter_id'=> $id));
}

function getNewsPost($id){
  return getDatabase()->one('SELECT * FROM nh_news WHERE news_id=:news_id', array(':news_id'=> $id));
}

function getWeatherConditions($id){
  return getDatabase()->one('SELECT * FROM nh_weather WHERE weather_id=:weather_id', array(':weather_id'=> $id));
}

function setImage(){
  // Upload successfull?
  if($_FILES["image"]["error"] > 0){ 
    return "Error";
  }else{
    
    // Move file to upload folder
    move_uploaded_file($_FILES["image"]["tmp_name"], getConfig()->get('imageDir') . "/" . $_FILES["image"]["name"]);
    // Add file to DB
    $image_id = getDatabase()->execute('INSERT INTO nh_image(filename, ev, aperture, shutter) VALUES (:filename, :ev, :aperture, :shutter)', array(':filename'=>$_FILES["image"]["name"], ':ev'=>'', ':aperture'=>'', ':shutter'=>''));
    
    // Twitter
    $twitter_id = getTwitter();
    // Weather
    $weather_id = getWeather();
    // News
    $news_id = getNews();

    // Add all ID's to the hour table
    // Time now, round down
    $rounded_unix = time() - (time() % 3600); // Round it down, to the hour
    $day_id = (strtotime(date("Y-m-d")) - strtotime("2012-1-1")) / (60 * 60 * 24);

    if(date('H', $rounded_unix) == 0){
    	$day_id = firstHour($rounded_unix);
    }
    getDatabase()->execute('INSERT INTO nh_hour(timestamp, image_id, day_id, news_id, twitter_id, weather_id) VALUES (:timestamp, :image_id, :day_id, :news_id, :twitter_id, :weather_id)', array(':timestamp'=>unixToMYSQL( $rounded_unix ), ':image_id'=>$image_id, ':day_id'=>$day_id, ':news_id'=>$news_id, ':twitter_id'=>$twitter_id, ':weather_id'=>$weather_id));
    return "Succes";
  }
}

function getTwitter(){
	$trendsRaw = file_get_contents('https://api.twitter.com/1/trends/23424909.json');
	$trends = json_decode($trendsRaw);
	$topTrend = $trends[0]->{'trends'}[0]->{'name'};
	return getDatabase()->execute('INSERT INTO nh_twitter(hash) VALUES (:hash)', array(':hash'=>$topTrend));
}

function getWeather(){
	$airpressure = 0;
	$airhumidity = 0;
	$temperature = 0;
	$windchill = 0;
	$rainrate = 0;
	$windspeed = 0;
	$winddirection = 0;

	return getDatabase()->execute('INSERT INTO nh_weather(airpressure, airhumidity, temperature, windchill, rainrate, windspeed, winddirection) VALUES (:airpressure, :airhumidity, :temperature, :windchill, :rainrate, :windspeed, :winddirection)', array(':airpressure'=>$airpressure, ':airhumidity'=>$airhumidity, ':temperature'=>$temperature, ':windchill'=>$windchill, ':rainrate'=>$rainrate, ':windspeed'=>$windspeed, ':winddirection'=>$winddirection));
}

function getNews(){
	$newsRaw = file_get_contents('http://www.nu.nl/feeds/rss/algemeen.rss');
	$x = simplexml_load_string($newsRaw);
	$title = (string)$x->channel->item[0]->title;
	$description = (string)$x->channel->item[0]->description;
	$image = (string)$x->channel->item[0]->enclosure['url'];

	return getDatabase()->execute('INSERT INTO nh_news(title, description, image) VALUES (:title, :description, :image)', array(':title'=>$title, ':description'=>$description, ':image'=>$image));
}

function firstHour($time){
	// Get the sunrise
	//date_sunrise(time(),SUNFUNCS_RET_STRING,38.4,-9,90,1)
	$sunrise = date_sunrise($time, SUNFUNCS_RET_TIMESTAMP, 52.049534661101355, 4.189910888671875);
	$sunset = date_sunset($time, SUNFUNCS_RET_TIMESTAMP, 52.049534661101355, 4.189910888671875);
	$sun_id = getDatabase()->execute('INSERT INTO nh_sun(sunrise, sunset) VALUES (:sunrise, :sunset)', array(':sunrise'=>$sunrise, ':sunset'=>$sunset));


	// Create a day
	$days = (strtotime("2012-1-1") - strtotime(date("Y-m-d"))) / (60 * 60 * 24);
    getDatabase()->execute('INSERT INTO nh_day(day_id, image_id, twitter_id, news_id, weather_id, sun_id) VALUES (:day_id, :image_id, :twitter_id, :news_id, :weather_id, :sun_id)', array(':day_id'=>$day, ':image_id'=>0, ':twitter_id'=>0, ':news_id'=>0, ':weather_id'=>0, ':sun_id'=>$sun_id));
	return $days;
}

function apiVersion(){
  return '1.0';
}

function tables(){
  return getDatabase()->all('SHOW TABLES;');
}

// HELPERS
function unixToMYSQL($unix){
  return date( 'Y-m-d H:i:s', $unix );
}

?>